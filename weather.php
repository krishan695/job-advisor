<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Select | Location</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script>
      $(document).ready(function(){
        $.ajax({
          url: "http://stsmentor.com/locationapi/locationApi.php?type=getCountries",
          success: function(data){
            var country = "<select onchange='getState();' class='form-control count'>";
            $.each(data['result'],function(key, value){
              $.each(value, function(keys, vals) {
                country += "<option value='"+keys+"'>"+vals+"</option>";
              });
            });
            country += "</select>";
            $('.country').html(country);
          }
        });
      });
      function getState(){
        var a = $('.count').val();
        $.ajax({
          url: "http://stsmentor.com/locationapi/locationApi.php?type=getStates&countryId="+a,
          success: function(data){
            var state = "<select onchange='getCity();' class='form-control stat'>";
            $.each(data['result'],function(key, value){
              $.each(value, function(keys, vals) {
                state += "<option value='"+keys+"'>"+vals+"</option>";
              });
            });
            state += "</select>";
            $('.state').html(state);
          }
        });
      }
      function getCity(){
        var a = $('.stat').val();
        $.ajax({
          url: "http://stsmentor.com/locationapi/locationApi.php?type=getCities&stateId="+a,
          success: function(data){
            var city = "<select class='form-control cit'>";
            $.each(data['result'],function(key, value){
              $.each(value, function(keys, vals) {
                city += "<option>"+vals+"</option>";
              });
            });
            city += "</select>";
            $('.city').html(city);
          }
        });
      }
    </script>
  </head>
<body>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="country mt-3"></div>
        <div class="state mt-3"></div>
        <div class="city mt-3"></div>
      </div>
    </div>
  </div>
</body>
</html>
