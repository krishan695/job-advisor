<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Start |Project</title>
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">
    <script src="assets/jquery.js"></script>
    <script>
    $(document).ready(function(){
      $('#login').hide();
      $('#log').click(function(e){
        $('#register').hide();
        e.preventDefault();
        $('#login').toggle(function(){
          $('#user').focus();
          $('#fetch').submit(function(e){
            e.preventDefault(e);
            var user = $('#user').val();
            var pass = $('#pass').val();
            $('#user').val('');
            $('#pass').val('');
            $('#user').focus();
            $.ajax({
              url: 'db/read.php',
              type: 'POST',
              data: {username:user,password:pass},
              success: function(data){
                alert(data);
              }
            });
          });
        });
      });
      $(document).scroll(function(){
        $('#login').hide();
      });
      $('#register').hide();
      $('#regis').click(function(e){
        $('#login').hide();
        e.preventDefault();
        $('#register').toggle(function(){
          $('#name').focus();

        });
      });
      $(document).scroll(function() {
        $('#register').hide();
      });
    });

    </script>
    <style media="screen">
      #login
      {
        position: fixed;
        right: 0px;
        top: 100px;
        z-index:9999999999;
        display: none;
      }
      #register
      {
        position: fixed;
        right: 0px;
        top: 100px;
        z-index:9999999999;
        display: none;
      }
      .row2
      {
        background: url('images/back.jpg');
        position: relative;
        width:100%;
        height:750px;
			  background-repeat: no repeat;
        background-size: 100% 100%;
      }
      .abcd
      {
        position: absolute;
        background: rgba(0, 0, 0, 0.5);
        width: 100%;
        height: 100%;
      }
      .a
      {
        height: 250px;
      }
      .input1
      {
        width:60% !important;
        height:60px !important;
        float: right;
        box-shadow:none !important;
      }
      .last
      {
        background: url('images/last5.jpg');
        width:100%;
        height: 400px;
        background-repeat: no repeat;
        background-size: 100% 100%;
      }
      .email
      {
        position: absolute;
      }
      .subs
      {
        position: absolute;
      }
      @media screen and (max-width:500px){
        .d
        {
          position: static !important;
        }
        .a
        {
          height: 0px;
        }
        .input1
        {
          float: none;
          margin: 0px;
          width: 100% !important;
        }
        table
        {
          display: none;
        }
        .image
        {
          width: 100%;
        }
        .shadow
        {
          text-shadow: 1px 1px #000;
        }
        .email
        {
          position: static;
        }
        .subs
        {
          position: static;
          margin-top: 10px;
          margin-left: 70px;
        }
        .center
        {
          text-align: center !important;
        }
        .navbar-brand
        {
          text-align: center;
        }
      }
    </style>
  </head>
  <body>
    <!-- Navbar start here -->
    <div class="container-fluid">
      <div class="row fixed-top d">
        <div class="col-md-4 col-sm-4 bg-dark">
          <nav class="navbar">
            <a href="" class="navbar-brand pl-md-5"><img src="images/log1.png" style="width:50%;height:80px;" /></a>
          </nav>
        </div>
        <div class="col-md-4 col-0 bg-dark ">

        </div>
        <div class="col-md-4 col-sm-4 bg-dark">
          <nav class="navbar navbar-expand-md">
						<ul class="navbar-nav">
							<li class="navbar-item">
								<a href="" class="nav-link pt-4  m-md-2 text-white" id="log">Login</a>
							</li>
							<li class="navbar-item">
								<a href="" class="nav-link  m-md-2 pt-md-4 text-white" id="regis">Registration</a>
							</li>
              <li class="navbar-item">
								<a href="recruter.php" class="nav-link  m-md-2 pt-md-4 text-white">Recruter Login/Signup</a>
							</li>
						</ul>
					</nav>
        </div>
      </div>
      <!--Navbar ends here-->
      <!--Second row start here-->
      <div class="row">
        <div class="col-md-12 px-0 row2">
          <div class="abcd">
            <?php include_once('register.php');?>
            <?php include_once('login.php');?>
            <div class="a"></div>
            <h3 class="display-4 text-center text-white mx-auto mt-5">Search Now &</h3>
            <h1 class=" display-3 text-center text-white mx-auto "><b>Get Your Dream Job</b></h1>
            <div class="container">
              <div class="row">
                <div class="col-md-6 col-sm-6 mx-md-auto my-md-5 px-0">
                  <input type="text" class="form-control input1" placeholder="Position"/>
                </div>
                <div class="col-md-3 mx-auto my-2 my-md-5 px-0">
                  <?php include_once('test.php'); ?>
                </div>
                <div class="col-md-3 col-sm-3 my-4 text-center mx-sm-auto my-md-5  px-0">
                  <input type="submit" class="btn" style="background: darkcyan;height:60px;width:50%;box-shadow:none !important;" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Second row ends here -->
      <!-- third row start here -->
      <div class="row bg-light">
        <div class="container">
          <div class="row">
            <div class="col-md-3 mt-5 p-3">
              <i class="far fa-lemon mb-3" style="color: cyan;font-size:40px;"></i>
              <h4>Food Service</h4>
              <p style="color:lightgrey;">Food service includes<br/> facilities that serve meals and<br/> snacks for immediate<br/> consumption on site (away<br/> from home).</p>
            </div>
            <div class="col-md-3 mt-5 p-3">
              <i class="fas fa-store mb-3" style="color: cyan;font-size:40px;"></i>
              <h4>Trade</h4>
              <p style="color:lightgrey;">Trade is a perfect option if you have just graduated and are looking for a job where your skills will be useful.</p>
            </div>
            <div class="col-md-3 mt-5 p-3">
              <i class="fas fa-video mb-3" style="color: cyan;font-size:40px;"></i>
              <h4>Media</h4>
              <p style="color:lightgrey;">Working in media sphere (TV companies, radio stations newspapers etc.) can also be a perfect career start.</p>
            </div>
            <div class="col-md-3 mt-5 p-3">
              <i class="fas fa-plane-departure mb-3" style="color: cyan;font-size:40px;"></i>
              <h4>Transportation</h4>
              <p style="color:lightgrey;">Transportation is closely connected with the movement of goods or people from one location to another.</p>
            </div>
          </div>
        </div>
      </div>
      <!-- third row ends here -->
      <!-- fourth row start here -->
      <div class="row bg-light">
        <div class="container">
          <div class="row">
            <div class="col-md-3 mt-5 py-3">
              <i class="fas fa-desktop mb-3" style="color: cyan;font-size:40px;"></i>
              <h4>IT Sphere</h4>
              <p style="color:lightgrey;">IT sphere is one of the most rapid-growing industries where many young specialists find a chance to be employed.</p>
            </div>
            <div class="col-md-3 mt-5 py-3">
              <i class="far fa-heart mb-3" style="color: cyan;font-size:40px;"></i>
              <h4>Medical</h4>
              <p style="color:lightgrey;">Working in this industry requires a great amount of dedication and expertise in medical treatment.</p>
            </div>
            <div class="col-md-3 mt-5 py-3">
              <i class="fas fa-wrench mb-3" style="color: cyan;font-size:40px;"></i>
              <h4>Industrial</h4>
              <p style="color:lightgrey;">Working in this sector is great for those people who want to experience real challenges and get a competitive salary.</p>
            </div>
            <div class="col-md-3 mt-5 py-3">
              <i class="fas fa-bullhorn mb-3" style="color: cyan;font-size:40px;"></i>
              <h4>PR & Marketing</h4>
              <p style="color:lightgrey;">This industry is a good choice if you wish to be employed at a company working with businesses and entrepreneurs.</p>
            </div>
          </div>
        </div>
      </div>
      <!-- fourth row ends here -->
      <!-- fifth row start here -->
      <div class="row">
        <div class="container">
          <div class="row">
            <div class="col-md-12 px-0 text-center">
              <h2 class="mt-3">Latest Job Positions</h2>
              <p class="mt-4" style="font-size:20px;color:lightgrey;">Browse the recent vacancies added by companies from all over the USA. Our database is<br/> updated every hour with hundreds of new job positions, which might interest you.</p>
            </div>
            <div class="col-md-12 px-0">
              <table class="table table-hover table-white mt-3 " style="color:lightgrey;" >
                <thead>
                  <tr>
                    <td>Date</td>
                    <td>Company</td>
                    <td>Job Vacancy</td>
                    <td>City</td>
                    <td>Salary</td>
                    <td>Employment</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Apr 27, 2017</td>
                    <td> <img src="images/1.jpg" height="50" width="65"> </td>
                    <td style="color:cyan;">Developer</td>
                    <td>New York</td>
                    <td>$6000</td>
                    <td>Full Time </td>
                  </tr>
                  <tr>
                    <td>Apr 27, 2017</td>
                    <td><img src="images/2.jpg" height="50" width="65"></td>
                    <td style="color:cyan;">Designer</td>
                    <td>San Francisco</td>
                    <td>$5500</td>
                    <td>Full Time </td>
                  </tr>
                  <tr>
                    <td>Apr 27, 2017</td>
                    <td><img src="images/3.jpg" height="50" width="65"></td>
                    <td style="color:cyan;">Manager</td>
                    <td>Chicago</td>
                    <td>$6000</td>
                    <td>Full Time </td>
                  </tr>
                  <tr>
                    <td>Apr 27, 2017</td>
                    <td><img src="images/4.jpg" height="50" width="65"></td>
                    <td style="color:cyan;">Marketer</td>
                    <td>New York</td>
                    <td>$4000</td>
                    <td>Full Time </td>
                  </tr>
                  <tr>
                    <td>Apr 27, 2017</td>
                    <td><img src="images/5.jpg" height="50" width="65"></td>
                    <td style="color:cyan;">Developer</td>
                    <td>Boston</td>
                    <td>$5500</td>
                    <td>Full Time </td>
                  </tr>
                  <tr>
                    <td>Apr 27, 2017</td>
                    <td><img src="images/6.jpg" height="50" width="65"></td>
                    <td style="color:cyan;">Art Director</td>
                    <td>San Diego</td>
                    <td>$6000</td>
                    <td>Full Time </td>
                  </tr>
                </tbody>
              </table>
              <div class="text-center">
                <input type="submit" value="VIEW ALL JOB POSITIONS" class="btn btn-outline-info my-4 px-5 py-3" style="border-radius:1px darkcyan;"/>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- fifth row ends here -->
      <!-- sixth row start here -->
      <div class="row">
        <div class="col-md-8 py-5 " style="background: black;">
          <h1 class="text-md-right text-white center" style="font-family: 'Open Sans Condensed', sans-serif;">Find the<b> Job of Your Dream</b> in a Couple of Clicks!</h1>
        </div>
        <div class="col-md-4 py-5 text-md-left text-center" style="background: black;">
          <input type="submit" class="btn text-white py-md-2 px-5 center" value="VIEW ALL VACANCIES" style="background: darkcyan;">
        </div>
      </div>
      <!-- sixth row ends here -->
      <!-- seventh row start here -->
      <div class="row">
        <div class="container">
          <div class="row">
            <div class="col-md-3 col-6 bg-white p-5 text-center">
              <i class="fas fa-map-marker-alt mb-3" style="color: cyan;font-size:40px;"></i>
              <h1>128</h1>
              <p style="color:lightgrey;">Cities</p>
            </div>
            <div class="col-md-3 col-6 bg-white p-5 text-center">
              <i class="far fa-copy mb-3" style="color: cyan;font-size:40px;"></i>
              <h1>45 954</h1>
              <p style="color:lightgrey;">Posted Vacancies</p>
            </div>
            <div class="col-md-3 col-6 bg-white p-5 text-center">
              <i class="far fa-star mb-3" style="color: cyan;font-size:40px;"></i>
              <h1>19</h1>
              <p style="color:lightgrey;">Top Companies</p>
            </div>
            <div class="col-md-3 col-6 bg-white p-5 text-center">
              <i class="far fa-user mb-3" style="color: cyan;font-size:40px;"></i>
              <h1>20 000+</h1>
              <p style="color:lightgrey;">Posted Resumes</p>
            </div>
          </div>
        </div>
      </div>
      <!-- seventh row ends here -->
      <!-- eight row start here -->
      <div class="row">
        <div class="col-md-6 bg-light text-right py-md-5">
          <img src="images/office.jpg" class="image py-5" alt="">
        </div>
        <div class="col-md-6 bg-light pb-5 p-md-5">
          <h3 class="pl-5 pt-md-5">A Few Words About Us</h3>
          <p class="pl-5 py-md-4" style="color:lightgrey;font-size:18px;">JobPortal is a global, end-to-end human capital solutions<br/> company focused on helping employers find, hire and<br/> manage great talent.<br/><br/>
          Combining advertising, software and services, we lead the<br/> industry in recruiting solutions, employment screening and<br/> human capital management. It also operates top job sites<br/> around the world.</p>
          <input type="submit" value="LEARN MORE" class="btn mx-5 px-5 py-3" style="background:darkcyan;" />
        </div>
      </div>
      <!-- eight row ends here -->
      <!-- ninth row start here -->
      <div class="row">
        <div class="col-md-12 bg-danger" >
          <h1 class="text-center mt-5">Testimonials</h1>
        </div>
        <div class="col-md-12 bg-danger">
          <?php include_once('index.html'); ?>
        </div>
      </div>
      <!-- ninth row ends here -->
      <!-- tenth row start here -->
      <div class="row">
        <div class="col-md-8 py-3 py-md-5 " style="background: black;">
          <h1 class="text-md-right text-center text-white" style="font-family: 'Open Sans Condensed', sans-serif;">We Offer Free Job Postings for <b>Employers</b></h1>
        </div>
        <div class="col-md-4 py-3 text-md-left text-center" style="background: black;">
          <input type="submit" class="btn text-white my-md-4 py-md-2 px-5" value="FIND OUT MORE" style="background: darkcyan;">
        </div>
      </div>
      <!-- tenth row ends here -->
      <!-- eleventh row start here -->
      <div class="row">
        <div class="col-md-12 last pt-3 p-md-5">
          <h1 class="text-center" style="font-family: 'Open Sans Condensed', sans-serif;">Newsletter</h1>
          <p class="text-center mb-3 p-3 shadow" style="color:grey;font-size:20px;">Keep up with our always upcoming news and updates. Enter your e-<br/>mail and subscribe to our newsletter.</p>
          <div class="row">
            <div class="col-md-6 mx-auto">
              <input type="text" class="form-control email" placeholder="Your e-mail.." style="height:50px;box-shadow: none !important;" />
              <input type="submit" value="SUBSCRIBE" class="btn subs px-5" style="right:-15px;height:50px;box-shadow: none !important;background: darkcyan;" />
            </div>
          </div>
        </div>
      </div>
      <!-- eleventh row ends here -->
      <!-- twelveth row start here -->
      <div class="row"style="background:darkslategrey;">
        <div class="col-md-8 col-sm-12 p-5">
          <h3 style="color:lightslategrey;" class="pl-4">Quick Links</h3>
            <hr class="ml-4" style="border-bottom: 1px solid lightgrey !important;">
            <div class="row text-white">
              <div class="col-md-3 col-sm-3 pl-5">
                <p><a href="" style="color:white;">service</a></p>
                <p><a href="" style="color:white;">single service</a></p>
                <p><a href="" style="color:white;">contacts</a></p>
                <p><a href="" style="color:white;">testimonials</a></p>
                <p><a href="" style="color:white;">term of use</a></p>
          	  </div>
              <div class="col-md-3 col-sm-3 pl-5">
                <p><a href="" style="color:white;">block</a></p>
                <p><a href="" style="color:white;">about us</a></p>
                <p><a href="" style="color:white;">about me</a></p>
                <p><a href="" style="color:white;">single project</a></p>
                <p><a href="" style="color:white;">single job</a></p>
              </div>
              <div class="col-md-3 col-sm-3 pl-5">
                <p><a href="" style="color:white;">careers</a></p>
                <p><a href="" style="color:white;">portfolio</a></p>
                <p><a href="" style="color:white;">single project</a></p>
                <p><a href="" style="color:white;">our history</a></p>
                <p><a href="" style="color:white;">single job</a></p>
              </div>
            	<div class="col-md-3 col-sm-3 pl-5">
                <p><a href="" style="color:white;">facebook</a></p>
                <p><a href="" style="color:white;">instagram</a></p>
                <p><a href="" style="color:white;">twiter</a></p>
                <p><a href="" style="color:white;">linkedin</a></p>
                <p><a href="" style="color:white;">pinterest</a></p>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-9 p-5" style="background:darkslategrey;">
            <h3 style="color:lightslategrey;">Contact Information</h3>
            <hr style="border-bottom: 1px solid lightgrey !important;">
            <div class="row">
              <div class="col-md-1 col-sm-1">
                <i class="fas fa-map-marker-alt text-white"></i>
              </div>
              <div class="col-md-11 col-sm-11">
                <p class="" style="color:lightgrey;">2130 Fulton Street San Diego, CA <br/>94117-1080 USA</p>
              </div>
            </div>
            <div class="row">
              <div class="col-md-1 col-sm-1">
                <i class="fas fa-phone text-white"></i>
              </div>
              <div class="col-md-11 col-sm-11">
                <p class="" style="color:lightgrey;">1-800-1234-678 <br/>1-800-8765-098</p>
              </div>
            </div>
            <div class="row">
              <div class="col-md-1 col-sm-1">
                <i class="fas fa-envelope-square text-white"></i>
              </div>
              <div class="col-md-11 col-sm-11">
                <p class="" style="color:lightgrey;">info@demolink.org</p>
              </div>
            </div>
          </div>
        </div>
      <!-- twelveth row ends here -->
    </div>
  </body>
</html>
