<link href="assets/css/bootstrap.css" rel="stylesheet"/>
<link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet">
<style>
  body
  {
    font-family: 'Lora', serif ;
  }
  input
  {
    height: 50px;
  }
</style>
<div class="col-md-4 bg-dark mx-auto mt-2 p-5" id="login">
  <h2 class="text-white">Sign In</h2>
  <form id="fetch">
  <?php
    if(isset($_GET['status']))
    {
      echo "<div class='alert'>".$_GET['status']."</div>";
    }
  ?>
  <input type="email" id="user" class="form-control mt-4" placeholder="E-mail" required />
  <input type="password" id="pass" class="form-control mt-4" placeholder="Password" required />
  <input type="submit" class="btn btn-success mt-4" value="SIGN IN" />
  </form>
</div>
