<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Select | Location</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <style media="screen">
      .stat
      {
        width: 100% !important;
        height: 60px !important;
        box-shadow: none !important;
      }
      .cit
      {
        width: 100% !important;
        height: 60px !important;
        box-shadow: none !important;
      }
    </style>
    <script>
    function getCountryId(cc){
      $.ajax({
        url:'http://stsmentor.com/locationapi/locationApi.php?type=getCountries',
        success: function(data){
          $.each(data['result'],function(key, value){
            $.each(value, function(keys, vals) {
              if(vals==cc){
                getState(keys);
              }
            });
          });

        }
      });
    }
    function getState(cc){
      // alert(cc);
      $.ajax({
        url: "http://stsmentor.com/locationapi/locationApi.php?type=getStates&countryId="+cc,
        success: function(data){
          var state = "<select onchange='getCity();' class='form-control stat'>";
          $.each(data['result'],function(key, value){
            $.each(value, function(keys, vals) {
              state += "<option value='"+keys+"'>"+vals+"</option>";
            });
          });
          state += "</select>";
          $('.state').html(state);
        }
      });
    // });
  }
  function getCity(){
    var a = $('.stat').val();
    $.ajax({
      url: "http://stsmentor.com/locationapi/locationApi.php?type=getCities&stateId="+a,
      success: function(data){
        var city = "<select class='form-control cit'>";
        $.each(data['result'],function(key, value){
          $.each(value, function(keys, vals) {
            city += "<option>"+vals+"</option>";
          });
        });
        city += "</select>";
        $('.city').html(city);
      }
    });
  }
      $(document).ready(function(){
        $.ajax({
          url: 'http://ip-api.com/json',
          success: function(data){
            // alert(JSON.stringify(data));
            country = data.country;
            getCountryId(country);
          }
        });
      });
    </script>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="state">

          </div>
          <div class="city mt-3">

          </div>
        </div>
      </div>
    </div>
  </body>
</html>
