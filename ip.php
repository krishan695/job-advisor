<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
  <meta charset="utf-8">
  <title>Get Country</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" />
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script>
    var country = '';
    function abcd(cc)
    {
      $.ajax({
        url: 'https://restcountries.eu/rest/v2/name/'+cc+'?fullText=true',
        success: function(data){
          // country = alert(JSON.stringify(data[0].flag));
          $('#country_name').html(data[0].name);
          $('#country_flag').html("<img src='"+data[0].flag+"' style='width:100%;'/>");
        }
      });
    }
    $(document).ready(function(){
      $.ajax({
        url: 'http://ip-api.com/json',
        success: function(data){
          country = data.country;
          abcd(country);
        }
      });
    });
  </script>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-md-2 text-light">
          <table class="">
            <tbody>
              <tr>
                <!-- <td>Name</td> -->
                <td id='country_name'></td>
              </tr>
              <!-- <tr>
                <!-- <td>Flag</td> -->
                <!-- <td id='country_flag'></td> -->
              <!-- </tr> --> 
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </body>
</html>
