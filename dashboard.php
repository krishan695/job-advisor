<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Dashboard | User</title>
    <link rel="stylesheet" href="assets/css/bootstrap.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" >
    <style media="screen">
      .col
      {
        position: relative;
      }
      .auto
      {
        position: absolute;
      }
      .button
      {
        position: absolute;
      }
    </style>
  </head>
  <body class="bg-white">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2 bg-light">
          <nav class="navbar">
            <a href="" class="navbar-brand"><img src="images/log1.png" style="width:100%;height:50px;" /></a>
          </nav>
        </div>
        <div class="col-md-7 bg-light">
          <nav class="navbar navbar-expand">
            <ul class="navbar-nav">
              <li class="navbar-item">
                <a href="" class="nav-link p-3" style="color: lightgrey;">Search Jobs</a>
              </li>
              <li class="navbar-item">
                <a href="" class="nav-link p-3" style="color: lightgrey;">Jobs For You</a>
              </li>
              <li class="navbar-item">
                <a href="" class="nav-link p-3" style="color: lightgrey;">Mailbox</a>
              </li>
              <li class="navbar-item">
                <a href="" class="nav-link p-3" style="color: lightgrey;">Profile</a>
              </li>
            </ul>
          </nav>
        </div>
        <div class="col-md-3 bg-light">
          <nav class="navbar navbar-expand">
            <ul class="navbar-nav">
            <li class="navbar-item">
              <a href="" class="nav-link p-3" style="color: lightgrey;"><i class="far fa-bell" style="font-size: 30px;"></i></a>
            </li>
            <li class="navbar-item">
              <a href="" class="nav-link p-3"><input type="submit" class="btn btn-outline-info px-5 py-2" value="Hi, Rahul" /></a>
            </li>
            </ul>
          </nav>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col">
          <input type="text" name="" class="form-control mt-5 mx-auto auto" style="width:80%;height:40px;" />
          <button type="submit" class="btn px-3 button"><i class="fas fa-search"></i></button>
        </div>
      </div>
      <div class="row">
        <div class="col-md-7">

        </div>
        <div class="col-md-4">

        </div>
      </div>
    </div>
  </body>
</html>
