  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" crossorigin="anonymous">
  <link href="css/bootstrap.min.css" rel="stylesheet"/>
  <link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet">
  <style>
  body
  {
  font-family: 'Lora', serif ;
  }
  input
  {
      height: 50px;
  }
  </style>
  <div class="col-md-4 bg-dark mx-auto mt-2 p-5" id="register">
   <h2 class="text-white">Registration</h2>
    <form id="insert">
      <?php
        if(isset($_GET['status']))
        {
          echo "<div class='alert'>".$_GET['status']."</div>";
        }
      ?>
    <input type="text" id="name" class="form-control mt-4" placeholder="Name" required />
    <input type="email" id="username" class="form-control mt-4" placeholder="Username" required />
    <input type="password" id="password" class="form-control mt-4" placeholder="Password" required />
    <input type="password" id="con_password" class="form-control mt-4" placeholder="Confirm Password" required />
    <input type="submit" class="btn btn-success mt-5" value="CREATE AN ACCOUNT" />
   <div class="row mt-4">
    <div class="col-md-4">
     <hr class="border-light"/>
    </div>
    <div class="col-md-4">
     <p class="text-light">or enter with</p>
    </div>
    <div class="col-md-4">
     <hr class="border-light"/>
    </div>
   </div>
</form>
<div class="btn-group btn-group-lg ">
  <button type="button" class="btn btn-primary ml-4 px-5" style="background: blue"><i class="fab fa-facebook"></i></button>
  <button type="button" class="btn btn-primary px-5"><i class="fab fa-twitter-square"></i></button>
  <button type="button" class="btn btn-danger px-5"><i class="fab fa-google"></i></i></button>
</div>
  <script src="assets/jquery.js"></script>
  <script>
  $('#insert').submit(function(e){
    e.preventDefault(e);
    var name = $('#name').val();
    var username = $('#username').val();
    var password = $('#password').val();
    var con_password = $('#con_password').val();
    $('#name').val('');
    $('#username').val('');
    $('#password').val('');
    $('#con_password').val('');
    $('#name').focus();
    $.ajax({
      url: 'db/insert.php',
      type: 'POST',
      data: {name:name,username:username,password:password,con_password:con_password},
      success: function(data){
        alert(data);
      }
    });
  });
  </script>
</div>
